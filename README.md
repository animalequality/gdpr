# Animal Equality GDPR plugin
Provides a cookie banner manager to use for countries with strict cookie requirements. The consent banner controls first-party-cookies to store the consent for an arbitrary list of consents.

It is able to block embedded third party content if consent is needed before loading it.

## Config
The configuration for each country and website are shipped within this plugin in the `config` folder and is selected during runtime based on the AE_COUNTRY constant and the currently active Wordpress theme.

```
title:
  # Type: string
  # Title shown on the banner

description: |
  # Description
  # Type: String (multiline)
  # Purpose: Provides a detailed message or disclaimer text that is displayed to the user.
  # It supports HTML tags and can contain line breaks.

imprint:
  label: 
    # Type: String
    # Purpose: The label text that will be displayed for the imprint link (usually "Impressum" or "Legal").
  url: 
    # Type: String (URL)
    # Purpose: The URL to the imprint or legal notice page. This should be a relative or absolute URL.

googleTagManager:
  dependencies:
    # Type: Array of Strings
    # Purpose: Lists the required consents for Google Tag Manager. Each element in the array must be the ID of a source.

embeds:
  overlay:
    button:
      cssClasses: 
        # Type: String
        # Purpose: CSS class names applied to style the overlay button.
      label: 
        # Type: String
        # Purpose: The label text for the button that allows the user to load embedded content.
    checkbox:
      label: 
        # Type: String
        # Purpose: The label text for a checkbox that allows users to automatically load content from a particular provider in the future.
    message:
      text: 
        # Type: String (multiline)
        # Purpose: A text message explaining the consequences of loading embedded content. Supports placeholders such as {source} and {link}.
      link:
        label: 
          # Type: String
          # Purpose: The label for a hyperlink in the message text.
        url: 
          # Type: String (URL)
          # Purpose: The URL that the hyperlink in the message text points to.

  sources:
    # Type: Array of Objects
    # Purpose: A list of third-party content providers that can be embedded (e.g., YouTube, Vimeo) and will be blocked until the consent for each dependency is given. currently this only supports sources that are embedded via iframe and contain the source id as part of the iframe src URL.
    # Each source has the following fields:
    - id: 
        # Type: String
        # Purpose: A unique identifier for the content provider.
      name: 
        # Type: String
        # Purpose: The display name of the content provider.
      dependencies: 
        # Type: Array of Strings
        # Purpose: An array of dependencies needed for this content provider. Each element in the array must be the ID of a source.

controls:
  position: 
    # Type: String ("top", "bottom")
    # Purpose: Defines the position of the control buttons on the screen.
  buttons:
    # Type: Array of Objects
    # Purpose: A list of buttons that allow users to interact with the consent controls. Each button has the following fields:
    - label: 
        # Type: String
        # Purpose: The label text for the button.
      action: 
        # Type: String ("accept", "decline", "personalize-enter", "personalize-save")
        # Purpose: The action performed when the button is clicked.
      cssClasses: 
        # Type: String
        # Purpose: CSS class names used to style the button.

sources:
  # Type: Array of Objects
  # Purpose: An arbitrary list of third-party services or technologies (e.g., Google Tag Manager, YouTube) involved in data processing or tracking. Each entry has its own toggle on the banner and can be separately consented by the user.
  # Each service has the following fields:
  - id: 
      # Type: String
      # Purpose: A unique identifier for the service.
    name: 
      # Type: String
      # Purpose: The display name of the service.```
