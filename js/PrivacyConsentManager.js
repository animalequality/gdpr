import Cookies from 'js-cookie';
import {handleEmbeds} from './embeds';

const SUPPORTED_EMBED_PROVIDERS = {
  youtube: {embedElement: 'iframe'},
  vimeo: {embedElement: 'iframe'},
  flickr: {embedElement: 'img', overlayContainer: '.wp-block-embed__wrapper'}
};

export default class PrivacyConsentManager {
  /**
   * @constructor
   */
  constructor() {
    this.consentManagerElement = document.querySelector('.cookie-consent-manager');
    this.consentSources = [...this.consentManagerElement.querySelectorAll('input[type="checkbox"]:not([data-no-cookie])')];
    this.buttons = this.consentManagerElement.querySelectorAll('.cookie-consent-manager__buttons button');
    this.buttonPersonalizeEnter = this.consentManagerElement.querySelector('button[data-action="personalize-enter"]');
    this.buttonPersonalizeSave = this.consentManagerElement.querySelector('button[data-action="personalize-save"]');
    this.isPersonalizing = false;
    this.googleTagManagerWasLoaded = false;
    this.cookieExpires = 365;
    this.cookiePrefix = 'ae_cookie_consent_';

    try {
      this.embedConfig = JSON.parse(this.consentManagerElement.dataset.embedConfig);
      this.embedConfig.sources = this.embedConfig.sources.map(source => ({...source, ...SUPPORTED_EMBED_PROVIDERS[source.id]}));
    } catch (error) {
      this.embedConfig = null;
      console.warn('PrivacyConsentManager: no embed config provided');
    }

    try {
      this.googleTagManagerConfig = JSON.parse(this.consentManagerElement.dataset.googleTagManagerConfig);
    } catch (error) {
      this.googleTagManagerConfig = null;
      console.warn('PrivacyConsentManager: no google tag manager config provided');
    }
  }

  /**
   * Returns a specific source checkbox
   * @param {string} source - The source to return
   * @returns {DomElement} - Source checkbox
   */
  _getSourceCheckbox(source) {
    return this.consentSources.find(sourceCheckbox => sourceCheckbox.name === source) || null;
  }

  /**
   * Writes the current status to the cookies and fires the consent given event
   * @returns {void}
   */
  _updateConsent() {
    this._updateStatistics();
    this._writeCookies();
    this._consentGiven();
    this._hide();
  }

  /**
   * The state is valid if a cookie exists for each source
   * @returns {boolean} - Is the cookie state valid
   */
  _stateIsValid() {
    return this.consentSources.filter(checkbox => this._readCookie(checkbox.name)).length === this.consentSources.length;
  }

  /**
   * Writes the given cookie
   * @param {DomElement} sourceCheckbox - The checkbox to write the cookie for
   * @returns {void}
   */
  _writeCookie(source, value) {
    Cookies.set(this.cookiePrefix + source, value, {expires: this.cookieExpires});
  };

  /**
   * Writes the cookie for each source checkbox based on the checkbox state
   * @returns {void}
   */
  _writeCookies() {
    this.consentSources.forEach(sourceCheckbox => {
      this._writeCookie(sourceCheckbox.name, sourceCheckbox.checked);
    });
  };

  /**
   * Returns the value of the cookie specified in the parameter
   * @param {string} source - The source to return the cookie value for
   * @returns {null || string}
   */
  _readCookie(source) {
    return Cookies.get(`${this.cookiePrefix}${source}`);
  };

  /**
   * Reads the cookie for each source and updates the state of the checkbox
   * @returns {void}
   */
  _readCookies() {
    this.consentSources.forEach(sourceCheckbox => {
      sourceCheckbox.checked = this._readCookie(sourceCheckbox.name) === 'true';
    });
  }

  /**
   * Dispatches the consent given event on window
   * @returns {void}
   */
  _consentGiven() {
    const eventName = 'aePrivacyConsentGiven';
    const event = new Event(eventName);

    this._readCookies();
    window.dispatchEvent(event);

    if (this.embedConfig) {
      handleEmbeds.call(this);
    }

    if (window.dataLayer) {
      window.dataLayer.push({
        event: eventName
      });
    }
  }

  /**
   * Bind links that open the consent manager
   * @return {void}
   */
  _bindOpenLink() {
    for (const link of document.querySelectorAll('.ae-cookie-consent-manager-open')) {
      link.classList.remove('hide');
      link.addEventListener('click', event => {
        event.preventDefault();
        this._show();
      });
    }
  }

  /**
   * Enters the mode to personalize the settings
   * @return {void}
   */
  _enterIsPersonalizing() {
    this.consentManagerElement.classList.add('is-personalizing');
    this.buttonPersonalizeEnter.style.display = 'none';
    this.buttonPersonalizeSave.style.display = 'block';
    this.isPersonalizing = true;
  }

  /**
   * Exits the mode to personalize the settings
   * @return {void}
   */
  _exitIsPersonalizing() {
    this.consentManagerElement.classList.remove('is-personalizing');
    this.buttonPersonalizeEnter.style.display = 'block';
    this.buttonPersonalizeSave.style.display = 'none';
    this.isPersonalizing = false;
  }

  /**
   * Consents to a specific source directly
   * @param  {string} source - Source to consent to
   * @returns {void}
   */
  _consentToSources(sources) {
    sources.forEach(source => {
      const checkbox = this._getSourceCheckbox(source);

      if (!checkbox) {
        return;
      }

      checkbox.checked = true;
      this._writeCookie(checkbox.name, checkbox.checked);
    });

    if (this._stateIsValid()) {
      this._consentGiven();
    }
  }

  /**
   * Hides the consent manager window
   * @returns {void}
   */
  _hide() {
    this.consentManagerElement.style.display = 'none';
    document.documentElement.style.overflow = 'auto';
  }

  /**
   * Shows the consent manager window after pulling the consent cookies
   * @returns {void}
   */
  _show() {
    const computedStyle = getComputedStyle(this.consentManagerElement);

    this._readCookies();
    this.consentManagerElement.style.display = 'block';

    if (computedStyle.display === 'none' ||
      computedStyle.visibility === 'hidden' ||
      computedStyle.width === '0px' ||
      computedStyle.height === '0px' ||
      parseInt(computedStyle.zIndex) < 0) {
      return;
    }

    document.documentElement.style.overflow = 'hidden';
  }

  /**
   * Sends a POST request with the current consents to track anonymously the consent behaviour of users
   * @returns {void}
   */
  async _updateStatistics() {
    try {
      const consents = this.consentSources.filter(sourceCheckbox => sourceCheckbox.checked).map(sourceCheckbox => sourceCheckbox.name);
      const response = await fetch('/wp-json/ae-consent-manager/v1/update-statistics', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(['sessions', ...consents])
      });

      if (!response.ok) {
        throw new Error(`Error: ${response.statusText}`);
      }
    } catch (error) {
      console.error('Error incrementing consent counters:', error);
    }
  }

  /**
   * Binds the element event click listeners and initially shows the ccm or calls the consentGiven function
   * depending on the current consent state
   */
  init() {
    this.buttons.forEach(button => {
      const action = button.dataset.action;

      button.addEventListener('click', (event) => {
        event.preventDefault();

        switch (action) {
          case 'decline':
            this.consentSources.forEach(sourceCheckbox => { sourceCheckbox.checked = false; });
            this._updateConsent();
            this._exitIsPersonalizing();
            break;
          case 'consent':
            this.consentSources.forEach(sourceCheckbox => { sourceCheckbox.checked = true; });
            this._updateConsent();
            this._exitIsPersonalizing();
            break;
          case 'personalize-enter':
            this._enterIsPersonalizing();
            break;
          case 'personalize-save':
            this._updateConsent();
            this._exitIsPersonalizing();
            break;
        }
      });
    });

    if (this._stateIsValid()) {
      this._consentGiven();
    } else {
      handleEmbeds.call(this);
      this._show();
    }

    this._bindOpenLink();
    this._exitIsPersonalizing();
  }

  /**
   * Returns if a specific source was consented
   * @param  {string} source - source to return consent status for
   * @returns {boolean}
   */
  hasConsent(source) {
    return this._stateIsValid() && this._readCookie(source) === 'true';
  }

  /**
   * Returns if consent was given to google tag manager based on the config
   * @returns {boolean}
   */
  hasGoogleTagManagerConsent() {
    if (!this.googleTagManagerConfig?.dependencies?.length) {
      return true;
    }

    const logic = this.googleTagManagerConfig.logic;
    const consentedDependencies = this.googleTagManagerConfig.dependencies.filter(dependency => this.hasConsent(dependency)).length;

    return !logic || logic === 'and' ? consentedDependencies === this.googleTagManagerConfig.dependencies.length : consentedDependencies > 0;
  }
};
