<tr>
    <td>
        <label class="name" for="cookie-consent-manager-<?php echo esc_html($source['id']); ?>">
        <?php echo esc_html($source['name']); ?>
        </label>
    </td>
    <td>
        <label for="cookie-consent-manager-<?php echo esc_html($source['id']); ?>">
            <?php echo !empty($source['description']) ? esc_html($source['description']) : ''; ?>
        </label>
    </td>
    <td>
        <input
            type="checkbox"
            id="cookie-consent-manager-<?php echo esc_html($source['id']); ?>"
            name="<?php echo esc_html($source['id']); ?>"
            value="true"
            <?php if (!empty($source['mandatory']) && $source['mandatory'] === true) {
                echo 'data-no-cookie="true" disabled checked';
            } ?>
        />
    </td>
</tr>

